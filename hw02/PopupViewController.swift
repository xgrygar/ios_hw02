//
//  PopupViewController.swift
//  hw02
//
//  Created by Adam Grygar on 10/03/2020.
//  Copyright © 2020 FI MUNI. All rights reserved.
//

import Foundation
import UIKit

class PopupViewController: UIViewController {
    
    @IBAction func closePopup(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
