//
//  ViewController1.swift
//  hw02
//
//  Created by Adam Grygar on 10/03/2020.
//  Copyright © 2020 FI MUNI. All rights reserved.
//

import Foundation
import UIKit

class FirstTab: UIViewController {
    
    @IBOutlet weak var resizableView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func changeHeight(_ slider: UISlider) {
        for constraint in resizableView.constraints {
            if constraint.identifier == "heightConstraint" {
                constraint.constant = CGFloat(slider.value)
            }
        }
        resizableView.layoutIfNeeded()
    }
    
}
