//
//  CustomStaticCellTableViewCell.swift
//  hw02
//
//  Created by Adam Grygar on 10/03/2020.
//  Copyright © 2020 FI MUNI. All rights reserved.
//

import UIKit

class CustomStaticCellTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
